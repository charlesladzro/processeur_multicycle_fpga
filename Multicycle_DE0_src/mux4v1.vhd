library ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;


entity mux4v1 IS
	generic (N: natural:=32);
	port ( 
		A,B,C,D	: in std_logic_vector((N-1) downto 0);
		COM	: in std_logic_vector(1 downto 0);
		S  	: out std_logic_vector((N-1) downto 0)
	);
END mux4v1 ;


architecture BEHAVIOUR of mux4v1 is
  
  begin
  with COM select
      S <= A when "00",
           B when "01",
           C when "10",
           D when  "11",
           (others => '0') when others;
end BEHAVIOUR;