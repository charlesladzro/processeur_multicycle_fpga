library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--------------------------------------------------------------
-- dÃƒÂ©claration des entrÃƒÂ©es
--------------------------------------------------------------
entity RegLd is 
port (
	  Data_in3: in std_logic_vector(31 downto 0);
	  RAZ,clk,PCWrEn: in std_logic ;  
	  Data_out3: out std_logic_vector(31 downto 0)	
	  );
end RegLd;

--------------------------------------------------------------
-- Architecture 
--------------------------------------------------------------

architecture RTL of RegLd is 
	signal reg_psr : std_logic_vector(31 downto 0);
begin
	process (clk, RAZ)
	 begin  
		-- remise à zero asynchrone
		if (RAZ='1') then
			Data_out3 <= (others =>'0');
		 -- Lorsque l'horloge est active sur un front montant
		elsif rising_edge(clk) then 
			if (PCWrEn='1') then 
				-- memorisation 
				reg_psr <= Data_in3;			
			end if;
		end if;
	end process; 

	Data_out3<= reg_psr;
	
end architecture;