library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------
-- declaration des entrees
--------------------------------------------------------------
entity VIC is 
port (
	--ENTREES
	  clk,Rst,IRQ_SERV,IRQ0,IRQ1: in std_logic ;  
	--SORTIES
	  IRQ : out std_logic;
	  VICPC : out std_logic_vector(31 downto 0)
	  );
end VIC;

--------------------------------------------------------------
-- Architecture 
--------------------------------------------------------------

architecture RTL of VIC is 

signal IRQ0_memo, IRQ1_memo  : std_logic:='0';
signal IRQ0_ech, IRQ1_ech : std_logic_vector(1 downto 0):="00";

begin

IRQ <= IRQ0_memo or IRQ1_memo;

-- dans cette partie notre systeme échantillonne une valeur 
-- qu'il mémorise dans irq0 et irq1
echantillonnage:process (clk,Rst)
 begin  
	if(Rst='1')then
		IRQ0_ech<="00";
		IRQ1_ech<="00";
	elsif ( rising_edge (clk)) then 
		IRQ0_ech(1) <= IRQ0_ech(0);
		IRQ0_ech(0) <= IRQ0;
		
		IRQ0_ech(1) <= IRQ0_ech(0);
		IRQ0_ech(0) <= IRQ1;
	end if;	
 end process;

-- Dans cette partie on compare la valeur mémorisé précédemment à la
-- à la nouvelle valeur rentré. Si c'est 0 on le met dans IRQ0_memo
-- et si c'est 1 on le met dans IRQ1_memo
gest_signal:process(IRQ0_ech, IRQ1_ech,Rst)
 begin
	if(Rst='1')then
		IRQ1_memo<='0';
		IRQ0_memo<='0';
	elsif ( IRQ0_ech="01") then 
		IRQ0_memo<='1';
	elsif ( IRQ_SERV='1') then
		IRQ0_memo<='0';
	end if;
	if ( IRQ1_ech="01") then 
		IRQ1_memo<='1';	
	elsif ( IRQ_SERV='1') then
		IRQ1_memo<='0';
	end if;
	end process;
	
-- Dans cette partie on gere les sortie et on force les événements
Gest_sortie:process(clk,Rst)
 begin
	if (Rst='1') then
		IRQ<='0';
		VICPC<=(others =>'0');
	elsif ( rising_edge (clk)) then 
		if (IRQ0_memo ='0' or IRQ1_memo ='1') then
			VICPC <=(others =>'0');
		elsif (IRQ0_memo<='1') then
			VICPC<=x"00000009";
		elsif (IRQ1_memo<='1') then
			VICPC<=x"00000015";
		end if;
	end if;
end process;
end architecture;

	