library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--------------------------------------------------------------
-- declaration des entrees
--------------------------------------------------------------
entity Reg is 
port (
		DATAIN: in std_logic_vector(31 downto 0);
		Rst,clk: in std_logic ;  
		DATAOUT: out std_logic_vector(31 downto 0)	
	  );
end Reg;

--------------------------------------------------------------
-- Architecture 
--------------------------------------------------------------

architecture RTL of Reg is 
begin
	process (clk, Rst)
	 begin  
	 
		-- remise a zero asynchrone
		if (Rst='1') then
			DATAOUT<= (others =>'0');
			
		 -- Lorsque l'horloge est active sur un front montant
		elsif rising_edge(clk) then 
			-- memorisation 
			DATAOUT <= DATAIN;			
		end if;
	end process;
  	
end architecture;