library IEEE;				-- type boolean, bit, character, severity_level
use IEEE.STD_LOGIC_1164.ALL;		-- definit les types std_logic , std_logic_vector , std_ulogic, les opÃ©rateurs logiques et les fonctions de recherches de fronts rising_edge() et falling_edge(). introduit par IEE 1164 en 1992.
use IEEE.NUMERIC_STD.ALL;		-- definit les sous-types entier "signed" ou "unsigned", introduit par IEEE 1076.3

entity RegBank is 
	port(
	--Definitions des variables d'entrees et sorties
		CLK	: IN 	STD_LOGIC ;
		W		: IN 	STD_LOGIC_VECTOR (31 DOWNTO 0);
		Ra		: IN 	STD_LOGIC_VECTOR (3 DOWNTO 0);
		Rb		: IN 	STD_LOGIC_VECTOR (3 DOWNTO 0);
		Rw		: IN 	STD_LOGIC_VECTOR (3 DOWNTO 0);
		We		: IN 	STD_LOGIC  := '1';
		A		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
		B		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)

	);
end entity;

architecture arc of RegBank is

	-- Declaration Type Tableau Memoire
	type table is array (15 downto 0) of std_logic_vector ( 31 downto 0);
	-- Fonction d'Initialisation du Banc de Registres
	function init_banc return table is
		variable result : table;
		begin 
			for i in 14 downto 0 loop
			result (i) := (others=>'0');
			end loop;
			result (15):=X"00000030";
			return result;
	end init_banc;

		-- Déclaration et Initialisation du Banc de Registres 16x32 bits
	signal Banc: table:=init_banc;
	
	begin

		process(CLK)
			begin 
				if rising_edge(CLK) then 
					if We = '1' then
						Banc(to_integer(unsigned(Rw)))<=W;
					end if;
				end if;

		end process;
		
		A <= Banc(to_integer(unsigned(Ra)));
		B <= Banc(to_integer(unsigned(Rb)));
end arc;