library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-----------------------------------------------------------------------------------------------------------------------
-- déclaration des entrées
-----------------------------------------------------------------------------------------------------------------------
entity mux2v1 is 
 generic (N: natural:=32);
 port(
	    Ax, Bx : in std_logic_vector((N-1) downto 0);
	    Com: in std_logic ;
	    Sx : out std_logic_vector((N-1) downto 0)
	);
end entity ;
------------------------------------------------------------------------------------------------------------------------
-- architecture
------------------------------------------------------------------------------------------------------------------------
architecture RTL of mux2v1 is 
  begin  
    with Com select
	        Sx<= Ax when'0',
	             Bx when others;
end architecture RTL;  